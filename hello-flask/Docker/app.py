from flask import Flask, logging as flog
import logging
from jsonformatter import JsonFormatter
import pprint, os

STRING_FORMAT = '''{
    "Name":            "name",
    "Levelno":         "levelno",
    "Levelname":       "levelname",
    "Pathname":        "pathname",
    "Filename":        "filename",
    "Module":          "module",
    "Lineno":          "lineno",
    "FuncName":        "funcName",
    "Created":         "created",
    "RelativeCreated": "relativeCreated",
    "Thread":          "thread",
    "ThreadName":      "threadName",
    "Process":         "process",
    "Message":         "message",
    "Timestamp": "asctime"
}'''
formatter = JsonFormatter(STRING_FORMAT)
#log = logging.getLogger('werkzeug')
#log.setLevel(logging.ERROR)

console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)

root = logging.getLogger()
root.addHandler(console_handler)

app = Flask(__name__)
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    flog.default_handler.setFormatter(logging.Formatter("%(message)s"))
    env=os.environ
    res = "<ul>"
    for elem in env:
     res+="<li>" +  elem+ "=" + env[elem]
    res+="</ul> <p> Files in /opt/data : </p>"
    if os.path.isdir("/opt/data"):
        files = os.listdir("/opt/data")
        res += "<ul>"
        for f in files:
            res += "<li>" + f
        res += "</ul>"
    return '<p>Bienvenu sur l\'appli hell-flask à la route : /' + path  +  '</p> <p> Env :' + res + "</p>"

