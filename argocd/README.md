# README

Application de argocd dans argocd. Ce dossier contient la configuration la plus simple pour avoir argocd dans argocd. 
Pour l'appliquer en dehors de l'interface argocd : `kubectl apply -k .`

Attention : le dossier argocd_helm est une autre manière de faire et je la déprécie.

Pour mettre à jour :

- Verifier les étapes nécessaires ici : https://github.com/argoproj/argo-cd/releases
- Changer la version dans le fichier `kustomization.yaml`
